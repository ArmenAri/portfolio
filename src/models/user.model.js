module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("user", {
        name: {
            type: Sequelize.STRING
        },
        job: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        location: {
            type: Sequelize.STRING
        }
    });

    return User;
};